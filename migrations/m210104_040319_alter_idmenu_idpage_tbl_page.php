<?php

use yii\db\Migration;

/**
 * Class m210104_040319_alter_idmenu_idpage_tbl_page
 */
class m210104_040319_alter_idmenu_idpage_tbl_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `page` CHANGE `idmenu` `idpage` INT(11) NOT NULL AUTO_INCREMENT;
        ALTER TABLE `post` CHANGE `idmenu` `idpage` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210104_040319_alter_idmenu_idpage_tbl_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210104_040319_alter_idmenu_idpage_tbl_page cannot be reverted.\n";

        return false;
    }
    */
}
