<?php

use yii\db\Migration;

/**
 * Class m201223_075218_add_kategori_tbl_post
 */
class m201223_075218_add_kategori_tbl_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql ="ALTER TABLE `post` ADD `id_kategori` INT(10) NULL AFTER `idpost`;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201223_075218_add_kategori_tbl_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_075218_add_kategori_tbl_post cannot be reverted.\n";

        return false;
    }
    */
}
