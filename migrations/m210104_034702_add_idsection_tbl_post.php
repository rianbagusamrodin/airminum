<?php

use yii\db\Migration;

/**
 * Class m210104_034702_add_idsection_tbl_post
 */
class m210104_034702_add_idsection_tbl_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `post` CHANGE `parent` `idsection` VARCHAR(10) NULL DEFAULT NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210104_034702_add_idsection_tbl_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210104_034702_add_idsection_tbl_post cannot be reverted.\n";

        return false;
    }
    */
}
