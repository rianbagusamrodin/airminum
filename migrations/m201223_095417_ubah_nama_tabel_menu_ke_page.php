<?php

use yii\db\Migration;

/**
 * Class m201223_095417_ubah_nama_tabel_menu_ke_page
 */
class m201223_095417_ubah_nama_tabel_menu_ke_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "alter table `menu` rename to `page`";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201223_095417_ubah_nama_tabel_menu_ke_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_095417_ubah_nama_tabel_menu_ke_page cannot be reverted.\n";

        return false;
    }
    */
}
