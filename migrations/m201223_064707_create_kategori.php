<?php

use yii\db\Migration;

/**
 * Class m201223_064707_create_kategori
 */
class m201223_064707_create_kategori extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "CREATE TABLE `kategori` (`id_kategori` INT(10) NOT NULL AUTO_INCREMENT, `nama` VARCHAR(30) NOT NULL , PRIMARY KEY (`id_kategori`)) ENGINE = InnoDB;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201223_064707_create_kategori cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_064707_create_kategori cannot be reverted.\n";

        return false;
    }
    */
}
