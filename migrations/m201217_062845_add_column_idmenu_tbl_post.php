<?php

use yii\db\Migration;

/**
 * Class m201217_062845_add_column_idmenu_tbl_post
 */
class m201217_062845_add_column_idmenu_tbl_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `post` ADD `idmenu` INT(11) NOT NULL AFTER `idpost`;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201217_062845_add_column_idmenu_tbl_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201217_062845_add_column_idmenu_tbl_post cannot be reverted.\n";

        return false;
    }
    */
}
