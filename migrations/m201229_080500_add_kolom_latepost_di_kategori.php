<?php

use yii\db\Migration;

/**
 * Class m201229_080500_add_kolom_latepost_di_kategori
 */
class m201229_080500_add_kolom_latepost_di_kategori extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql ="ALTER TABLE `kategori` ADD `id_latepost` INT NULL DEFAULT NULL AFTER `nama`;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201229_080500_add_kolom_latepost_di_kategori cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201229_080500_add_kolom_latepost_di_kategori cannot be reverted.\n";

        return false;
    }
    */
}
