<?php

use yii\db\Migration;

/**
 * Class m210105_044835_add_subtitle_tbl_post
 */
class m210105_044835_add_subtitle_tbl_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `post` ADD `subtitle` VARCHAR(100) NULL AFTER `title`;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210105_044835_add_subtitle_tbl_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210105_044835_add_subtitle_tbl_post cannot be reverted.\n";

        return false;
    }
    */
}
