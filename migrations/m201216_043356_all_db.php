<?php

use yii\db\Migration;

/**
 * Class m201216_043356_all_db
 */
class m201216_043356_all_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "
        CREATE TABLE `post` (
            `idpost` int(11) NOT NULL,
            `type` int(11) NOT NULL,
            `date` datetime NOT NULL,
            `title` text,
            `content` text,
            `file` varchar(1024) DEFAULT NULL,
            `image` varchar(1024) DEFAULT NULL,
            `parent` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          
          CREATE TABLE `image` (
            `idimage` int(11) NOT NULL,
            `name` text NOT NULL,
            `desc` text,
            `url` text NOT NULL,
            `file` varchar(1024) DEFAULT NULL,
            `ordering` int(11) DEFAULT NULL,
            `parent` int(11) DEFAULT NULL,
            `idmenu` int(11) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
              
          CREATE TABLE `menu` (
            `idmenu` int(11) NOT NULL,
            `type` int(11) DEFAULT NULL,
            `title` text NOT NULL,
            `content` text,
            `image` varchar(1024) DEFAULT NULL,
            `ordering` int(11) DEFAULT NULL,
            `parent` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
          
          
          ALTER TABLE `image`
            ADD PRIMARY KEY (`idimage`),
            ADD KEY `fk_image_menu_idx` (`idmenu`);
          
          --
          -- Indexes for table `menu`
          --
          ALTER TABLE `menu`
            ADD PRIMARY KEY (`idmenu`);
          
          --
          -- Indexes for table `post`
          --
          
          ALTER TABLE `post`
            ADD PRIMARY KEY (`idpost`);
          
          --
          -- AUTO_INCREMENT for table `image`
          --
          ALTER TABLE `image`
            MODIFY `idimage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
          
          --
          -- AUTO_INCREMENT for table `menu`
          --
          ALTER TABLE `menu`
            MODIFY `idmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
          
          --
          -- AUTO_INCREMENT for table `post`
          --
          ALTER TABLE `post`
            MODIFY `idpost` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
          
          --
          -- Constraints for table `image`
          --
          ALTER TABLE `image`
            ADD CONSTRAINT `fk_image_menu` FOREIGN KEY (`idmenu`) REFERENCES `menu` (`idmenu`) ON DELETE NO ACTION ON UPDATE NO ACTION;
          COMMIT;
        ";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201216_043356_all_db cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201216_043356_all_db cannot be reverted.\n";

        return false;
    }
    */
}
