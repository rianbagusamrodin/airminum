<?php

use yii\db\Migration;

/**
 * Class m201223_082824_modify_null_tbl_post
 */
class m201223_082824_modify_null_tbl_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `post` CHANGE `idmenu` `idmenu` INT(11) NULL;
        ALTER TABLE `post` CHANGE `type` `type` INT(11) NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201223_082824_modify_null_tbl_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_082824_modify_null_tbl_post cannot be reverted.\n";

        return false;
    }
    */
}
