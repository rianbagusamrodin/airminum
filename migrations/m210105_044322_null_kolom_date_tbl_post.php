<?php

use yii\db\Migration;

/**
 * Class m210105_044322_null_kolom_date_tbl_post
 */
class m210105_044322_null_kolom_date_tbl_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE `post` CHANGE `date` `date` DATETIME NULL DEFAULT NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210105_044322_null_kolom_date_tbl_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210105_044322_null_kolom_date_tbl_post cannot be reverted.\n";

        return false;
    }
    */
}
