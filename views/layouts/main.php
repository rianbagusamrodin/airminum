<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Page;
use app\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- my font montserrat-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800&display=swap" rel="stylesheet">
</head>
<style>
    .bg-light {
        background: transparent !important;
        transition: 0.5s ease;
    }

    .bg-light.scrolled {
        background: whitesmoke !important;
    }
    .navbar{
        font-family: 'PT Sans Narrow', sans-serif;
        font-size: 13px;
        color: black;
    }
</style>

<body>
    <?php $this->beginBody() ?>

    <?php
    $menus = Page::find()->all();

    foreach ($menus as $key => $data) {
        $title = strtolower(str_replace(' ', '_', $data['title']));
        $arrMenu[] = ['label' => $data['title'], 'url' => ['/site/' . $title]];
    }

    NavBar::begin([
        'brandLabel' => '<img src=' . Url::baseImg("logo_biru.png") . '>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => "navbar navbar-expand-lg navbar-default bg-light fixed-top"
        ],
    ]);
    echo Html::beginTag('div', ['class' => 'collapse navbar-collapse flex-column ml-lg-0 ml-3']);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ml-auto'],
        'items' => $arrMenu
    ]);
    ?>
        <ul class="navbar-nav flex-row ml-auto">
            <li class="nav-item">
                <a class="nav-link py-1 pr-3" href="#"><i class="fa fa-facebook"></i>IG</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 pr-3" href="#"><i class="fa fa-instagram"></i>fb</a>
            </li>
            <li class="nav-item">
                <a class="nav-link py-1 pr-3" href="#"><i class="fa fa-twitter"></i>youtube</a>
            </li>
            <form class="form-inline ml-auto">
                <input type="text" class="form-control mr-sm-2" placeholder="Search">
                <button type="submit" class="btn btn-outline-light">Search</button>
            </form>
        </ul>
    <?php
    echo Html::endTag('div');
    NavBar::end();
    ?>


    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <br />
    <?= $content ?>

    <?php $this->endBody() ?>
    <script>
        $(window).scroll(function() {
            $('nav').toggleClass('scrolled', $(this).scrollTop() > 100);
        });
    </script>
</body>

</html>
<?php $this->endPage() ?>