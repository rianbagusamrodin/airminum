<?php

use app\helpers\Url;
use app\helpers\Helper;
use app\models\Kategori;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;

?>
<style>
  .carousel {
    margin-top: 80px;
    position: relative;
  }

  .carousel-item .img-fluid {
    width:100%;
    height:100%;
  }

  .carousel .container {
    z-index: 1;
    position: relative;
  }

  /* .carousel::after{
    content:'';
    display: block;
    width: 100%;
    height: 100%; 
    background: -webkit-linear-gradient(top, transparent, black);
    position: absolute;
    bottom: 0;
  } */

  p, h1{
    font-family: 'Montserrat';
    color: black;
  }
  /* untuk btn edit di samping h3 tiap section */
  header > h3 { display: inline-block; }
  header span { margin-left: 20px; }

.img-fluid {
    max-width: 100%;
    height: auto;
    /* opacity: 0.6; */
}
</style>

<?php
$this->title = "Air Minum Biru";
?>

<!-- section 1 -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class=""></li>
    <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="2" class=""></li>
  </ol>
  <div class="carousel-inner">
  <?php foreach ($m_carousel as $index => $value){
    if($index == 0){
      echo Html::beginTag('div', ['class' => 'carousel-item active']);  
    }else {
      echo Html::beginTag('div', ['class' => 'carousel-item']);  
    }
      echo Html::img(Url::base() . '/uploads/' . $value['image'],['width' => '100%', 'height' => '500px']);
      echo Html::beginTag('div', ['class' => 'container']);
        echo Html::beginTag('div',['class' => 'carousel-caption text-left']);
          echo Html::tag('h1', $value['title']);
          echo Html::tag('p',$value['content']);
        echo Html::endTag('div');
      echo Html::endTag('div');
    echo Html::endTag('div');
  }?>

  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php 
  if(!Yii::$app->user->isGuest){
   echo Html::a('Edit Carousel', ['/admin/carousel/index'], ['class' => 'btn btn-sm btn-outline-secondary']); 
  }
?>

<br />

<!-- section 2 -->
<div class="container">
  <?php 
  // echo "<pre>";print_r($model2);die;
  if (!Yii::$app->user->isGuest) {
    echo Html::a('Edit', ['/admin/post/update', 'id' => $model2['idpost']], ['class' => 'btn btn-sm btn-outline-secondary']);
  } ?>
  <!-- <div style='background-image: url('. .')'> </div> -->

  <img src="http://localhost/airminum/web/uploads/tropi-flat1609833542.png" weight="100" height='476,856px'>

  <div style="background-image: url('localhost/airminum/web/uploads/tropi-flat1609833542.png');">fsdf
    <a href="#">Selengkapnya...</a>
  </div>
</div>

<!-- section 3 -->
<br />
<div class="container">
<header class="pb-3 mb-4 font-italic border-bottom">
    <h3>Product</h3>
    <?php if(!Yii::$app->user->isGuest){ ?>
      <span><a href="#">Edit</a></span>
    <?php } ?>
  </header>
  <div class="row">
    <?php
    foreach ($model3 as $key => $data) {
      echo Html::beginTag('div', ['class' => 'col-md-4']);
      echo Html::beginTag('div', ['class' => 'card mb-4 box-shadow']);
      echo Html::img(Url::base() . '/uploads/' . $data['image'], ['style' => 'height: 225px; width: 100%; display: block']);
      echo Html::beginTag('div', ['class' => 'card-body']);
      echo Html::tag('h4', $data['title']);
      echo Html::tag('p', $data['content']);
      if (!Yii::$app->user->isGuest) {
        echo Html::a('Edit', ['/admin/post/update', 'id' => $data['idpost']], ['class' => 'btn btn-sm btn-outline-secondary']);
      }
      echo Html::endTag('div');
      echo Html::endTag('div');
      echo Html::endTag('div');
    }
    ?>
    <!-- end foreach -->
  </div>
</div>

<!-- section 4 -->
<div class="container">
  <header class="pb-3 mb-4 font-italic border-bottom">
    <h3>Hot News</h3>
    <?php if(!Yii::$app->user->isGuest){ ?>
      <span><a href="#">Edit</a></span>
    <?php } ?>
  </header>
  <div class="row">
    <?php
    foreach ($model4 as $key => $data) {
      if($data['id_kategori'] == 1){
        $kat_warna = 'text-primary';
      }else if($data['id_kategori'] == 2){
        $kat_warna = 'text-success';
      }
      else if($data['id_kategori'] == 3){
        $kat_warna = 'text-warning';
      }
      else{
        $kat_warna = 'text-secondary';
      }
      echo Html::beginTag('div', ['class' => 'col-md-4']);
      echo Html::beginTag('div', ['class' => 'card mb-4 box-shadow']);
      echo Html::img(Url::base() . '/uploads/' . $data['image'], ['style' => 'height: 225px; width: 100%; display: block']);
      echo Html::beginTag('div', ['class' => 'card-body']);
      echo Html::tag('strong', ArrayHelper::getValue(Kategori::getList(),$data['id_kategori']), [ 'class' => "d-inline-block mb-2 <?= $kat_warna ?>"]);
      echo Html::tag('h3', $data['title']);
      echo Html::tag('div', date('d F Y', strtotime($data['date'])), ['class' => 'mb-1 text-muted']);
      echo Html::tag('p', mb_strimwidth($data['content'], 0, 100, "..."));
      echo Html::a('Continue reading',['/site/view', 'id' => $data['idpost']]);

        // echo Html::a('Edit', ['/admin/post/update', 'id' => $data['idpost']], ['class' => 'btn btn-sm btn-outline-secondary']);
      
      echo Html::endTag('div');
      echo Html::endTag('div');
      echo Html::endTag('div');
    }
    ?>
    <!-- end foreach -->
  </div>
</div>