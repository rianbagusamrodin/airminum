<?php
    use yii\web\JqueryAsset;
?>
    <style>
        header {
            padding: 156px 0 100px;
        }

        section {
            padding: 30px 0;
        }

        .sidebar {
            padding: 30px 0;
        }
    </style>

<body id="page-top">
    <div class="row">
        <div class="col-lg-2">
            <aside>
                <ul id="sidebarMenu" class="col-lg-12 d-md-block sidebar nav position-fixed">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#prolayanan" data-method="post">Produk dan Layanan</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#garansi" data-method="post">Garansi</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact" data-method="post">Contact</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#tes" data-method="post">lain-lain</a></li>
                </ul>
            </aside>
        </div>
        <div class="col-lg-10">
            <section id="layanan">
                <h2>layanan</h2>
                <p class="lead">This is a great place to talk about your webpage. This template is purposefully unstyled so you can use it as a boilerplate or starting point for you own landing page designs! This template features:</p>
                <ul>
                    <li>Clickable nav links that smooth scroll to page sections</li>
                    <li>Responsive behavior when clicking nav links perfect for a one page website</li>
                    <li>Bootstrap's scrollspy feature which highlights which section of the page you're on in the navbar</li>
                    <li>Minimal custom CSS so you are free to explore your own unique design options</li>
                </ul>
            </section>

            <section id="sdf">
                <h2>About this page</h2>
                <p class="lead">This is a great place to talk about your webpage. This template is purposefully unstyled so you can use it as a boilerplate or starting point for you own landing page designs! This template features:</p>
                <ul>
                    <li>Clickable nav links that smooth scroll to page sections</li>
                    <li>Responsive behavior when clicking nav links perfect for a one page website</li>
                    <li>Bootstrap's scrollspy feature which highlights which section of the page you're on in the navbar</li>
                    <li>Minimal custom CSS so you are free to explore your own unique design options</li>
                </ul>
            </section>
            <section id="garansi">
                <h2>Garansi</h2>
                <p class="lead">This is a great place to talk about your webpage. This template is purposefully unstyled so you can use it as a boilerplate or starting point for you own landing page designs! This template features:</p>
                <ul>
                    <li>Clickable nav links that smooth scroll to page sections</li>
                    <li>Responsive behavior when clicking nav links perfect for a one page website</li>
                    <li>Bootstrap's scrollspy feature which highlights which section of the page you're on in the navbar</li>
                    <li>Minimal custom CSS so you are free to explore your own unique design options</li>
                </ul>
            </section>
            <section id="services" class="bg-light">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <h2>Services we offer</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit inventore, expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur ex. Nemo assumenda laborum vel, labore ut velit dignissimos.</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <h2>Contact us</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et repudiandae ipsa exercitationem, in, quo totam.</p>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et repudiandae ipsa exercitationem, in, quo totam.</p>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et repudiandae ipsa exercitationem, in, quo totam.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tes">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <h2>Lain-lain</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et repudiandae ipsa exercitationem, in, quo totam.</p>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et repudiandae ipsa exercitationem, in, quo totam.</p>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et repudiandae ipsa exercitationem, in, quo totam.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <?php
    // <!-- Bootstrap core JavaScript -->
    $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jquery/jquery.min.js', ['depends' => [JqueryAsset::className()]]);
    $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/bootstrap/js/bootstrap.bundle.min.js', ['depends' => [JqueryAsset::className()]]);

    // <!-- Plugin JavaScript -->
    $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jquery-easing/jquery.easing.min.js', ['depends' => [JqueryAsset::className()]]);

    // <!-- Custom JavaScript for this theme -->
    $this->registerJsFile(Yii::$app->request->BaseUrl . '/js/scrolling-nav.js', ['depends' => [JqueryAsset::className()]]);
    ?>