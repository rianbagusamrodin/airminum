<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $idpost
 * @property int $type
 * @property string $date
 * @property string|null $title
 * @property string|null $content
 * @property string|null $file
 * @property string|null $image
 * @property int|null $idsection
 */
class Post extends \yii\db\ActiveRecord
{
    public $kolom_gmb;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'id_kategori'], 'integer'],
            [['date'], 'safe'],
            [['title', 'subtitle', 'content','idsection'], 'string'],
            [['file', 'image'], 'string', 'max' => 1024],
            // [['file', 'image'], 'file', 'skipOnEmpty' => TRUE, 'extensions' => 'jpg, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpost' => 'Idpost',
            'type' => 'Type',
            'date' => 'Date',
            'title' => 'Title',
            'content' => 'Content',
            'file' => 'File',
            'image' => 'Image',
            'idsection' => 'Nama Section',
            'id_kategori' => 'Kategori',
            'subtitle' => 'Sub Title',
        ];
    }
}
