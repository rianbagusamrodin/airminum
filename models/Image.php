<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property int $idimage
 * @property string $name
 * @property string|null $desc
 * @property string $url
 * @property string|null $file
 * @property int|null $ordering
 * @property int|null $parent
 * @property int $idmenu
 *
 * @property Menu $idmenu0
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url', 'idmenu'], 'required'],
            [['name', 'desc', 'url'], 'string'],
            [['ordering', 'parent', 'idmenu'], 'integer'],
            [['file'], 'string', 'max' => 1024],
            [['idmenu'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['idmenu' => 'idmenu']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idimage' => 'Idimage',
            'name' => 'Name',
            'desc' => 'Desc',
            'url' => 'Url',
            'file' => 'File',
            'ordering' => 'Ordering',
            'parent' => 'Parent',
            'idmenu' => 'Idmenu',
        ];
    }

    /**
     * Gets query for [[Idmenu0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmenu0()
    {
        return $this->hasOne(Menu::className(), ['idmenu' => 'idmenu']);
    }
}
