<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "kategori".
 *
 * @property int $id_kategori
 * @property string $nama
 */
class Kategori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kategori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 30],
            [['id_latepost'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'nama' => 'Nama',
            'id_latepost' => 'Latepost',
        ];
    }
    public static function getList($id = null){
        $models = self::find()
                ->all();
        if($id){
        $models = self::find()
            ->andwhere(['id_kategori' => $id])
            ->all();
        }

        return ArrayHelper::map($models,'id_kategori','nama');
    }
}
