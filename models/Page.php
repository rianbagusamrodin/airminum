<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $idmenu
 * @property int|null $type
 * @property string $title
 * @property string|null $content
 * @property string|null $image
 * @property int|null $ordering
 * @property int|null $parent
 *
 * @property Image[] $images
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'ordering', 'parent'], 'integer'],
            [['title'], 'required'],
            [['title', 'content'], 'string'],
            [['image'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmenu' => 'Idmenu',
            'type' => 'Type',
            'title' => 'Title',
            'content' => 'Content',
            'image' => 'Image',
            'ordering' => 'Ordering',
            'parent' => 'Parent',
        ];
    }

    /**
     * Gets query for [[Images]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['idmenu' => 'idmenu']);
    }
}
