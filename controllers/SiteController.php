<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Post;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     * di section 2 dan 3 show data di home, bukan termasuk data post,
     * hanya numpang simpan data gambar, judul, content.
     * 
     * section 2: show data di bawahnya carousel, dengan id yg direserve 4.
     * section 3: id yg direserve 1,2,3.
     * section 4: show data post 3 data terbaru di sembarang kategori.
     */
    public function actionIndex()
    {
        $m_carousel = Post::find()
            ->andWhere(['idsection' => 'carousel'])
            ->all();

        $model_section2 = Post::find()
            ->andWhere(['in', 'idpost',[4]])
            ->one();

        $model_section3 = Post::find()
            ->andWhere(['in', 'idpost',[1,2,3]])
            ->all();

        $model_section4 = Post::find()
            ->andWhere(['is not ','id_kategori', null])
            ->andWhere(['!= ','id_kategori', 0])
            ->orderBy(['date' => SORT_DESC])
            ->limit(3)
            ->all();
            
        return $this->render('index',[
            'model2' => $model_section2,
            'model3' => $model_section3,
            'model4' => $model_section4,
            'm_carousel' => $m_carousel,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // return $this->goBack();
            return $this->render('/admin/layout/index');
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionFranchise()
    {
        return $this->render('/page/franchise');
    }
    public function actionProduk(){
        $req = Yii::$app->request;
        $model = Post::find()
            ->where(['idpage' => 2])
            ->all();

        return $this->render('/page/produk',[
            'model' => $model,
        ]);
    }
    public function actionMitra_baru(){
        return $this->render('/page/mitra_baru');
    }
    public function actionPromo_baru(){
        return $this->render('/page/promo_baru');
    }
    public function actionTerkini_biru(){
        return $this->render('/page/terkini_biru');
    }
    public function actionKontak(){
        return $this->render('/page/kontak');
    }
}
