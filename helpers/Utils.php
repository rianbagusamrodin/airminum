<?php

namespace app\helpers;

use app\models\Image;
use app\models\File;
use yii\bootstrap4\Html;
use yii\web\UploadedFile;

/**
 * Description of Utils
 *
 * @author Eko Wahyu Wibowo <bowpunya@gmail.com>
 * @since Jul 9, 2018
 * 
 */
class Utils
{

    //put your code here


    public static function compress($path, $file, $w = 0, $h = 0, $c = true, $q = 90)
    {

        $source = $path . $file;
        if (!is_file($source)) {
            return null;
        }
        if (!is_dir($path . 'thumb')) {
            mkdir($path . 'thumb');
        }

        $destination = './' . $path . 'thumb/' . 'th' . $w . 'x' . $h . '-' . $file;

        if (is_file($destination)) {
            return $destination . "?cache=true";
        }

        $orig = getimagesize($source);
        $pngQuality = round(abs(($q - 100) / 11.111111));
        list($ow, $oh) = $orig;
        $nw = $w;
        $nh = $h;

        if ($w > 0 && $h > 0) {
            $ratio_orig = $ow / $oh;

            if ($w / $h > $ratio_orig) {
                $nh = $w / $ratio_orig;
            } else {
                $nw = $h * $ratio_orig;
            }
            $r = TRUE;
        }

        if ($orig['mime'] == 'image/jpeg' || $orig['mime'] == 'image/gif') {
            $outmime = 'jpg';
            $image = $orig['mime'] == 'image/jpeg' ? imagecreatefromjpeg($source) : imagecreatefromgif($source);
            if ($r) {
                $thumb = imagecreatetruecolor($nw, $nh);
                imagecopyresampled($thumb, $image, 0, 0, 0, 0, $nw, $nh, $ow, $oh);
                imagejpeg($thumb, $destination, $q);
            } else {
                imagejpeg($image, $destination, $q);
            }
        } else if ($orig['mime'] == 'image/png') {
            $outmime = 'png';
            $image = imagecreatefrompng($source);
            imagealphablending($image, true);
            imagesavealpha($image, true);

            if ($r) {
                $thumb = imagecreatetruecolor($nw, $nh);
                imagealphablending($thumb, true);
                imagesavealpha($thumb, true);
                imagefill($thumb, 0, 0, 0x7fff0000);
                imagecopyresampled($thumb, $image, 0, 0, 0, 0, $nw, $nh, $ow, $oh);
                imagepng($thumb, $destination, $pngQuality);
            } else {
                imagepng($image, $destination, $pngQuality);
            }
        }

        if ($c) {
            $tmp = $outmime == 'png' ? imagecreatefrompng($destination) : imagecreatefromjpeg($destination);
            $xx = ($nw - $w) / 2;
            $yy = ($nh - $h) / 2;
            $cr = imagecrop($tmp, ['x' => $xx, 'y' => $yy, 'width' => $w, 'height' => $h]);
            if ($cr) {
                if ($outmime == 'png') {
                    imagepng($cr, $destination, $pngQuality);
                } else {
                    imagejpeg($cr, $destination, $q);
                }
                imagedestroy($cr);
            }
        }


        imagedestroy($image);
        imagedestroy($thumb);

        return $destination;
    }

    private static function doUpload($model, $file, $oldname, $path)
    {
        if ($file) {
            $safebase = preg_replace('/[^a-z0-9]+/', '-', strtolower($file->baseName));
            $newname = $safebase . time() . '.' . $file->extension;
            if ($file->saveAs('./' . $path . '/' . $newname)) {

                if (is_file('./' . $path . '/' . $oldname)) {
                    unlink('./' . $path . '/' . $oldname);
                }
                return $newname;
            }
        }

        return $oldname;
    }
    /*
    *   $key = untuk nama field image nya
    *   UploadedFile digunakan untk
    */

    public static function upload($model, $key, $oldname = null, $path = 'uploads')
    {
        $model->$key = UploadedFile::getInstance($model, $key);

        return self::doUpload($model, $model->$key, $oldname, $path);
    }

    public static function uploads($model, $key, $oldnames = null, $path = 'uploads')
    {
        $model->$key = UploadedFile::getInstances($model, $key);
        $ret = [];
        foreach ($model->$key as $i => $row) {
            $ret[] = self::doUpload($model, $row, isset($oldnames[$i]) ? $oldnames[$i] : null, $path);
        }

        return $ret;
    }

    public static function postCorner($parent)
    {
        return $parent == 2 ? 'brown' : '';
    }

    public static function readMore($content)
    {
        $id = rand() . time();
        $html = $content;
        $max = 500;
        if (strlen($content) > $max) {
            $has = strpos($content, "</p>");
            $html = substr($content, 0, $has ? $has + 4 : $max);
            $html .= "<div id='pos-$id' class='collapse'>" . substr($content, $has ? $has + 4 : $max) . "</div>";
            $html .= "<p class='text-right readmore'> " . Html::a('En savoir +', '#pos-' . $id, ['data' => ['toggle' => 'collapse']]) . "</p>";
        }
        return $html;
    }

    public static function deleteImage($id)
    {
        $model = Image::findOne($id);
        if ($model == null) {
            return null;
        }

        $filename = './uploads/' . $model->url;
        if (is_file($filename)) {
            unlink($filename);
        }

        if ($model->parent == 0) {
            $images = Image::findAll(['parent' => $id]);
            foreach ($images as $img) {
                $filename = './uploads/' . $img->url;
                if (is_file($filename)) {
                    unlink($filename);
                }
                $img->delete();
            }
        }

        $model->delete();
    }

    public static function deleteFile($id)
    {
        $model = File::findOne($id);
        if ($model == null) {
            return null;
        }

        $filename = './uploads/' . $model->file;
        if (is_file($filename)) {
            unlink($filename);
        }

        $model->delete();
    }


    public static function titleCut($title, $len = 18, $cut = '...')
    {
        return $title; //strlen($title) >= $len ? substr($title, 0, $len - 4) . $cut  : $title;
    }
}
