<?php

namespace helpers;

use Yii;
use yii\bootstrap4\Html;

class Helper{

    const STAT_ACTIVE = 1;
    const STAT_INACTIVE = 0;
    const SECTION_CAROUSEL = 'carousel';

    public static function fa($name) {
        return Html::tag('i', null, ['class' => "fa fa-$name"]);
    }

    public static function menusb($fa, $name) {
        return self::fa($fa) . ' ' . Html::tag('span', $name, ['class' => 'hide-menu']);
    }

    public static function flashSuccess($msg = null) {
        $m = empty($msg) ? "Proses Berhasil." : $msg;
        return Yii::$app->session->setFlash('success', $m);
    }

    public static function flashFailed($msg = null) {
        $m = empty($msg) ? "Gagal, silakan ulangi kembali." : $msg;
        return Yii::$app->session->setFlash('danger', $m);
    }

    /** Catatan function getMonths()
     * 
     * @param type $id = show bulan by id ex: getMonths(3) = Maret
     * @param type $id_bulan_urut = digunakan untuk menampilkan bulan yg dimulai dari inputan, bukan januari
     *                              case untuk pembayaran SPP, ex: getMonth(null, 4)= April - Maret
     * @param type $id_bulan_batas = show batas bulan
     *          ex: getMonths(null, 5, 8) = Mei - Agustus
     * 
     * @return ??
     */
    public static function getMonths($id = null, $id_bulan_urut = null, $id_bulan_batas = null) {
        $list = [
            1 => "Januari",
            2 => "Februari",
            3 => "Maret",
            4 => "April",
            5 => "Mei",
            6 => "Juni",
            7 => "Juli",
            8 => "Agustus",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Desember",
        ];
        //handle jika ada params $id_bulan_batas, untuk handle batas akhir dari loop bulan
        if (isset($list[$id_bulan_urut]) && isset($list[$id_bulan_batas])) {
            if ($id_bulan_urut > $id_bulan_batas) {
                for ($i = $id_bulan_urut; $i <= 12; $i++) {
                    $bln[$i] = $list[$i];
                }
                for ($i = 1; $i <= $id_bulan_batas; $i++) {
                    $bln[$i] = $list[$i];
                }
            } else if ($id_bulan_urut <= $id_bulan_batas) {
                for ($i = $id_bulan_urut; $i <= $id_bulan_batas; $i++) {
                    $bln[$i] = $list[$i];
                }
            }
            return $bln;
        }

        //handle jika params hanya id_bulan_urut
        if ($id_bulan_urut && isset($list[$id_bulan_urut])) {

            for ($i = (int) $id_bulan_urut; $i <= count($list); $i++) {
            //     $bln[$i] = $list[$i];
            }
            if ($id_bulan_urut > 1) {
                for ($a = 1; $a < $id_bulan_urut; $a++) {
                    $bln[$a] = $list[$a];
                }
            }
            return $bln;
        }
        //handle jika params hanya id nya, maka ditampilkan 
        if ($id) {
            $id = (int) $id;
            if (isset($list[$id])) {
                return $list[$id];
            }
            return key($list);
        }

        return $list;
    }

    public static function tgl_indo($tanggal = null, $isfull = false) {
        $inttime = $tanggal ? strtotime($tanggal) : time();
        $tanggal = $isfull ? date('Y-m-d H:i:s', $inttime) : date('Y-m-d', $inttime);

        $bulan = self::getMonths();
        $pecahkan = explode('-', $tanggal);
        $tgl_menit = explode(' ', $pecahkan[2]);
        $menit = '';
        if (count($tgl_menit) > 1) {
            $menit = $tgl_menit[1];
            $pecahkan[2] = $tgl_menit[0];
        }

        return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0] . ' ' . $menit;
    }
}