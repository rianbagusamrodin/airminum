<?php

namespace app\helpers;

use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class ButtonActionColumn extends ActionColumn {

    public $dropButtons;

    protected function initDefaultButtons() {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'View'),
                    'aria-label' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-primary btn-sm',
                    'style' => 'margin: 2px;'
                        ], $this->buttonOptions);
                return Html::a('<i class="glyphicon glyphicon-search"></i>', $url, $options);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Update'),
                    'aria-label' => Yii::t('yii', 'Update'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-primary btn-sm',
                    'style' => 'margin: 2px;'
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-danger btn-sm',
                    'style' => 'margin: 2px;'
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['printview'])) {
            $this->buttons['printview'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Cetak'),
                    'aria-label' => Yii::t('yii', 'Cetak'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-success btn-sm',
                    'style' => 'margin: 2px;'
                        ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-print"></span>', $url, $options);
            };
        }

        if (!isset($this->buttons['dropdown'])) {
            $this->buttons['dropdown'] = function ($url, $model, $key) {
                $list = "";
                if (is_array($this->dropButtons) && !empty($this->dropButtons)) {
                    foreach ($this->dropButtons as $i => $row) {
                        $title = is_numeric($i) ? $row : $i;
                        $options = array_merge([
                            'title' => Yii::t('yii', $title),
                            'aria-label' => Yii::t('yii', $title)
                                ], $this->buttonOptions);
                        $list .= Html::tag('li', Html::a($title, str_replace('dropdown', $row, $url), $options));
                    }
                }
                $html = '<div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="glyphicon glyphicon-cog"> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right"> ' . $list . ' </ul>
                          </div>';
                return $html;
            };
        }
    }

}
