<?php

namespace app\helpers;

use app\models\Menu;
use app\models\Post;

/**
 * Description of Url
 *
 * @author Eko Wahyu Wibowo <bowpunya@gmail.com>
 * @since May 1, 2018
 * 
 */
class Url extends \yii\helpers\Url {

    public static function baseImg($file = '') {
        return self::base(true) . '/img/' . $file;
    }

    public static function baseUploads($file = '', $w = 0, $h = 0) {
		
		if(empty($file) || !is_file('./uploads/' . $file)){
				$file = '404.jpg';
		}
		
        return $w > 0 && $h > 0 ? self::base(true) . '/' . Utils::compress('uploads/', $file, $w, $h) : self::base(true) . '/uploads/' . $file;
    }

    public static function idToSlug($id, $text) {
        return $id . '-' . strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), $text));
    }

    public static function imageBack($model) {
        return ['/admin/menu/view', 'id' => $model->idmenu];
    }
    
    public static function fileBack($model) {
        return ['/admin/code/view', 'id' => $model->idcode];
    }

}
