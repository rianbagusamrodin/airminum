<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\ckeditor\CKEditor;
use app\models\Kategori;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= Html::csrfMetaTags() ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <?php 
    // echo "<pre>";print_r($model->idpost);die; 
    if(!in_array($model->idpost, array(1,2,3,4,5))){
        echo $form->field($model, 'date')->widget(DatePicker::className(), [
            'options' => ['placeholder' => 'Pilih Tanggal ...'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'keepInvalid' => false,
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ]);

        echo $form->field($model, 'id_kategori')->dropDownList([0 => '--Pilih--'] + Kategori::getList());
    }
    ?>
    
    <?= $form->field($model, 'content')->widget(CKEditor::class, ['preset' => 'basic']) ?>
    
    <?= $form->field($model, 'kolom_gmb')->fileInput()->label('Image') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success text-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
