<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap4\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'label' => 'Date',
                'attribute' => 'date',
            ],
            'title:ntext',
            'type',
            'id_kategori',
            'content:ntext',
            'image',
            [
                'class' => '\kartik\grid\ActionColumn',
                // 'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>']
            ]
        ],
    ]); ?>


</div>