<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\ckeditor\CKEditor;
use app\models\Kategori;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= Html::csrfMetaTags() ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'subtitle')->textInput(); ?>
    
    <?= $form->field($model, 'content')->widget(CKEditor::class, ['preset' => 'basic']) ?>
    
    <?= $form->field($model, 'kolom_gmb')->fileInput()->label('Image') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success text-right']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
