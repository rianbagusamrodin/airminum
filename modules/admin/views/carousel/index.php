<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap4\Breadcrumbs;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Carousel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Carousel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'title:ntext',
            'content:ntext',
            'subtitle:ntext',
            [
                'attribute' => 'image',
                'label' => 'Image',
                'format' => 'raw',
                'value' => function ($data) {
                    $url = Url::base().'/uploads/'.$data->image;
                    return Html::img($url,['alt' => $data->image,'width' => '200','height' => '100']);
                }
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
                // 'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>']
            ]
        ],
    ]); ?>


</div>