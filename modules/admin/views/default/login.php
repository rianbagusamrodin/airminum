<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<br />
<br />
<div class="col-sm-4 d-flex mx-auto">
    <div class="text-center">
                <h1><?= $this->title = 'Admin Panel &dash; ' . Yii::$app->name;?></h1>
            <?php
                $form = ActiveForm::begin([
                    'id' => 'login-form',
                ]);
                ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group row">
                <div class="col-lg-12">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        </div>