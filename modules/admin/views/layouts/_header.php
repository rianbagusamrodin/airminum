<?php
use app\models\Page;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\helpers\Url;

            $menus = Page::find()->all();
            NavBar::begin([
                'brandLabel' => '<img src='.Url::baseImg("logo_biru.png").'>',
                'brandUrl' => Yii::$app->homeUrl,
            ]);
            
            foreach($menus as $key => $data){
                $title = strtolower(str_replace(' ', '_', $data['title']));
                $arrMenu[] = [
                    'label' => $data['title'], 
                    'url' => ['/site/'.$title]
                ];
            }
            if (!Yii::$app->user->isGuest) {
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav mr-auto position-fixed'],
                    'items' => $arrMenu,
                ]);
            }
            NavBar::end();
            ?>