<?php
/* @var $this View */
/* @var $content string */

use app\assets\AppAsset;
use app\helpers\Url;
use app\models\Menu;
use app\models\Post;
use app\widgets\Alert;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\BaseUrl;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        
        <script>
        var BASEURL = '<?= Url::base(TRUE) ?>';
        </script>
    </head>
    <body>
        <?php $this->beginBody() ?>

            <?= $this->render('_header') ?>
            <main>
                <div class="row">
                    <div class="col-md-2">
                    <?php 
                    if(!Yii::$app->user->isGuest){
                        echo $this->render('_sidebar'); 
                    }
                    ?>
                    </div>
                    <div class="col-md-10">
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </div>
                </div>
            </main>


        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
