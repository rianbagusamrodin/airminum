<?php

use helpers\Helper;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;

?>
<aside>
    <?php
    echo Nav::widget([
        'options' => [
            'class' => 'col-md-12 d-md-block sidebar position-fixed',
            'id' => 'sidebarMenu',
        ],
        'items' => [
            ['label' => 'Pages', 'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'Franchise', 'url' => ['/site/franchise']],
                ['label' => 'Produk', 'url' => ['/site/produk']],
                ['label' => 'Mitra Baru', 'url' => ['/site/mitra_baru']],
                ['label' => 'Promo Baru', 'url' => ['/site/promo_baru']],
                ['label' => 'Terkini Biru', 'url' => ['/site/terkini_biru']],
                ['label' => 'Kontak', 'url' => ['/site/kontak']],
                ]],
            ['label' => 'Post', 'url' => ['/admin/post']],
            ['label' => 'Master', 'items' => [
                ['label' => 'Kategori', 'url' => ['/admin/kategori']],
                ['label' => 'Page', 'url' => ['/admin/page']],
            ]],
            ['label' => 'Setting', 'items' => [
                ['label' => 'Site & Logo', 'url' => ['/admin/logo']],
                ['label' => 'Footer', 'url' => ['/admin/footer']],
                ['label' => 'Tema & Background', 'url' => ['/admin/theme']],
            ]],
            ['label' => 'Log Out', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
        ],
    ]);
    ?>
</aside>