<?php

namespace app\modules\admin\controllers;

use app\models\LoginForm;
use iutbay\yii2kcfinder\KCFinder;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['login'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    var $layout = "main.php"; // untuk hidden menu

    public function actionIndex() {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/site/index']);
        }
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $kcfOptions = array_merge(KCFinder::$kcfDefaultOptions, [
                'uploadURL' => Yii::getAlias('@web') . '/uploads',
                'access' => [
                    'files' => [
                        'upload' => true,
                        'delete' => false,
                        'copy' => false,
                        'move' => false,
                        'rename' => false,
                    ],
                    'dirs' => [
                        'create' => true,
                        'delete' => false,
                        'rename' => false,
                    ],
                ],
            ]);

            Yii::$app->session->set('KCFINDER', $kcfOptions);
            
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

}
