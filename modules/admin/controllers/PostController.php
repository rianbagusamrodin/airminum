<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Post;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\helpers\Utils;
use app\helpers\Helper;
use yii\web\UploadedFile;
use yii\db\Exception;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['login'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Post();
        $model->load(Yii::$app->request->queryParams);
        $dataProvider = new ActiveDataProvider([
            'query' => Post::find()
                ->andFilterWhere(['id_kategori' => $model->id_kategori])
                ->andFilterWhere(['type' => $model->type])
                ->andFilterWhere(['date' => $model->date])
                ->andFilterWhere(['like', 'title', $model->title])
                ->andFilterWhere(['like', 'content', $model->content])
                ->orderBy(['idpost' => SORT_DESC]),
                'pagination' => [ 'pageSize' => 5 ],
        ]);


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $req = Yii::$app->request;
        $model = new Post();

        if ($model->load($req->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $gambar = Utils::upload($model,'kolom_gmb');
                if($gambar){
                    $model->image = $gambar;
                }
                if ($model->save()) {
                    $transaction->commit();
                    return $this->redirect(['index']);
                }
               Helper::flashFailed();
            } catch (Exception $ex) {
                Helper::flashFailed($ex->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $req = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load($req->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $gambar = Utils::upload($model, 'kolom_gmb');
                if ($gambar){
                    $model->image = $gambar;
                }
                
                if ($model->save()) {
                    $transaction->commit();
                    return $this->redirect(['index']);
                }
                Helper::flashFailed();
            } catch (Exception $ex) {
                Helper::flashFailed($ex->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
